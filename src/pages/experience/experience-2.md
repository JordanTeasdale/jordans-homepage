---
layout: ../../layouts/MarkdownPostLayout.astro
title: "Digital Scorpion Interactive"
description: ""
image:
  url: "/images/DSILogo_thumbnail.webp"
  alt: "DSI Logo"
---
<!-- Description
Digital Scorpion Interactive is an Indie Game Development Studio founded in 2021.
-->

<img alt="DSI" src="/images/DSILogo_content.webp"/>

Digital Scorpion Interactive is an Indie Game Development Studio founded in 2021 by Nicholas Revell. He had a vision to change the way games are built and prove to the gaming world they are a force to be reckoned with. DSI was founded on the principles of Ingenuity, Integrity, and Innovation. They strive to break the boundaries of modern gaming and bring to life an immersive experience gamers will keep coming back for.

<img alt="TOV" src="/images/TOVLogo_content.webp"/>

### TALES OF VALORIS: SWALLOW'S DEFENDERS

Prepare to defend Swallows Keep as mobs of thieves, bandits, and monsters threaten the citizens of our grand city. Players will command a variety of defenders to halt these evildoers in their tracks. Use your arsenal of skilled fighters and mages to creatively defeat your foes and stop the heinous plans of the coalition of mysterious bandit commanders.

### Responsibilities
* Designed and implemented in-game defender selection UI.
* Created Defender Selection Screen.
* Added Game Manager to track player's progress.
* Created tutorial and dialogue systems.

### In-Game Defender Selection UI
No tower defense game would be complete without a way to place said towers. So, creating a simple and easy way for the player to select which defender they wanted to place in a specific tile was one of the first things I worked on for Tales of Valoris. I wanted to make sure that the system was dynamic and customizable so that it could easily be modified if any design or art changes happened in the future. Therefore, I created a radial-type menu to display the available defenders that could be changed to be larger or smaller, have more or fewer spaces for defenders, or have a set spacing between icons or dynamic spacing depending on the number of selected defenders. 

### Defender Selection Screen
The defender selection screen (not to be confused with the defender selection UI) had some difficulties in being developed in learning to use Unity’s UI Grid and Scroll View and locking down the correct over to move things with multithreading to make sure everything would move smoothly across the screen and to the correct positions. Since this game was primarily created to be a mobile game, I wanted to try and lower memory usage where I could. Therefore, I used a sort of bitmask to keep track of what defenders the player had selected. 

### Game Manager
The Game Manager was a dual effort between another developer and me. I primarily focused on the tracking of the player’s data during a play session and preserving necessary information across scenes while the other developer concentrated on saving and loading the player’s data when needed. We would track information like what defenders the player has selected, which levels have been beaten and on which difficulty, which enemies and defenders the player has seen to unlock their respective journal entries, and which achievements the player has earned. 

### Tutorial
For the tutorial, I wanted to create something that would clearly walk the player through all the major aspects of the gameplay, but I still wanted it to be made so that it could easily be changed if something needed to be added or removed. To do this, I made a sort of “array of steps” that would keep track of the dialogue to display, the condition to move to the next step, and a blocker to limit what the player could interact with to help ensure that the tutorial won’t break. I also created a dialogue system to be used both in the tutorial as well as in many other places in the game such as when a new defender or enemy is announced. I created this system in a way that made it very easy for the narrative team to add or replace dialogue in different places.

### Conclusion
Overall, working on Tales of Valoris was an amazing experience where I was able to learn a lot. I was able to work closely with members of other departments such as design, art, and narrative, and create features to help make their jobs easier. I am very proud of the work I contributed to this game. It is available on the Apple AppStore and Google Play Store, and Digital Scorpion Interactive recently announced that it will soon also be available on Steam.