---
layout: ../layouts/MarkdownKeiLayout.astro
title: "Kei Truck Simulator"
---

### SUMMARY

Kei Truck Simulator (official name still pending) is a currently solo indie game developed in Unreal Engine 5.3 using a mix of Blueprints and C++ code. The game centers around our main character who has recently moved to a small town and started their own much-needed delivery service. As they help deliver parcels and letters for neighbors and other small businesses, they learn more about the people of this small town and begin to form relationships with them. Before long, they become a full member of the community and leave their status as an "outsider" behind.

The two major components of this game that everything else is built on are the delivery truck and the NPC interactions. In addition to using UE's Chaos Vehicle system, the delivery vehicle, a Kei truck (hence the name) uses a custom-made inventory sorting system. The NPCs use dynamic, branching behavior trees which allows them to have custom dialogue and actions dependent on the choices of the player. 

### Responsibilities
* Designing the gameplay loop and the goals of the game.
* Creating the vehicle and player interactions with the world.
* Researching, designing, and creating the Inventory Sorting System.
* Creating NPC behaviors and dialogue interactions.
* Modeling models, texturing textures, and shading shaders.

### Inspiration for the Idea
Several years ago, I formed a bit of a fascination with Kei trucks. They are small, efficient, utilitarian trucks that are designed simply to do a job. They're simple, don't care about being glamorous, and kind of cute in a way.
Eventually, asked me, "If you love them so much, why don't you make a game about them," and so I got to work. I wanted to create a game that reflects the attributes of Kei trucks that I admire so much. I wanted it to be simple,
minimalistic in nature, but still offer a wide variety of experiences. 

I started with the idea of just a "Truck Driving Simulator, but Kei truck" where you just make deliveries from place to place. But to keep with the simplistic nature I am shooting for, I wanted to eliminate as many menus as possible
to keep the player inside the shoes of the character, which meant the player would have to interact with NPCs instead of menus. This led me to incorporate more ideas from games like Stardew Valley where the player can form
meaningful connections with the characters in the game and become a part of the community. These aspects have not only influenced the gameplay of making deliveries to and from small, local businesses and residents, but it has
also influenced the art style that I am aiming for. Although this is a first-person 3D game, I want to try and recreate that hand-drawn 2D aspect of not just RPG games, but also animated media, especially the Studio Ghibli movies.

### Vehicle and Player Interactions
<video autoplay muted loop disablepictureinpicture>
            <source src="/videos/KeiTruckBoxInteractions.mp4" type="video/mp4"/>
            <source src="mov_bbb.ogg" type="video/ogg">
        </video>
Driving the Kei truck and being able to pick up and move objects is quite literally half of the game, so I want to make sure that it is done right. For the vehicle, I used UE's new chaos vehicle physics system which will allow me
to have a great deal of customizability over the vehicles and will allow me to have multiple different brands of Kei trucks, each with their own unique strengths and weaknesses. A big hurdle in doing this was that the majority of
tutorials and examples are done in blueprints, and while this is good for prototyping functionality, I want the final product to be in C++ code (I am a programmer after all) so there was a bit of difficulty in properly
transferring the functionality to C++.

For the player interactions, I wanted to create physics interactions that the player could have fun with (and potentially exploit) without it getting in the way of the core gameplay. This meant allowing the player being able to push
boxes around and throw boxes at different objects, but disabling collisions under certain circumstances such as when the player is holding the box and when it is in an "inventory." 

### Inventory Sorting System

<img alt="KTS Truck Bed Inventory" src="/images/TruckBedInventory_content.webp"/>

Being the first major and game-specific feature I had to design, create, and add to the game, I needed to be sure that it encompassed as many of the core values of the game to set a proper staging ground for the rest of the features to
be added later on. I knew I wanted the truck bed to be "inventory-based" and not physics-based on holding the objects so that they wouldn't go bouncing out while driving. I wanted to system to be immersive, which meant eliminating as
as many menus as possible. So, the player is going to directly place the objects into the truck bed without having to deal with menus I also wanted the player to have quick and simple interactions with the truck bed, which meant taking the
job of organizing the truck bed away from the player so that they wouldn't have to worry about it.

So, I needed to create a system that would allow the player to place an object into the truck bed, calculate the "best" way to organize all the objects currently in the inventory including the new object, and then place the objects according
to the algorithm's outcome. At first, this seemed like it would be simple enough to accomplish, but as it would turn out, creating an efficient algorithm to pack 2D rectangles into a given 2D space is apparently far from being "solved."
After several days of research, I eventually found a recent research paper that laid out the creation of an algorithm that would relatively quickly and efficiently pack rectangles into a given 2D space as I was looking for. I was
then able to take inspiration from their algorithm to create my own. 

The inventory sorting algorithm for the truck bed is based on a two-level search algorithm described. It creates "hypothetical" positions where different boxes can be placed, uses a formula to determine which box in which position fits 
the best, and temporarily "adds" it to the inventory. It then repeats this process until either every box has been placed in the inventory, or until no more can be placed. If all the boxes can fit, then it confirms the positions 
of all the boxes and places the boxes into the inventory, otherwise, the system prevents the player from placing the box they are attempting to fit into the inventory.

There are still optimizations to be made to the overall algorithm, but I have a very strong and working foundation to begin with.

### NPC BEHAVIOR

<img alt="KTS NPC Behavior Tree" src="/images/NPCBehaviorTree_content.webp"/>

The NPCs in the game utilize UE's Behavior Tree system along with custom-made behavior nodes which allows for specific and customizable behaviors. Currently, NCPs are able to have different dialogue lines based on the choices of the 
player. Plans are to have the NPCs take in more information about what the player has done and allow the NPCs to have more options of actions to do based on what the player has done. I also wanted to carry the immersive aspect of the 
inventory in that I did not want menuing in how the player was to interact with the NPCs. So, I created a system where the player can select dialogue options without the use of their mouse. This allows that player to look and move
around while still being able to continue their conversations.

### ART STYLE

<img alt="KTS Shader" src="/images/KeiTruckShader_content.webp"/>

I want the art style to reflect the simplicity and coziness of the overall themes I am incorporating into the game. This meant shooting for a more hand-drawn/Studio Ghibli style as opposed to the more realistic style you would see in other
simulator games such as Truck Driving or Farming Simulator. I wanted to mimic the 2D aesthetic, but in a 3D game by using a combination of textures with the appearance of brush strokes and shaders to create the harsh shadows and outlines
one would often see in drawings.

As someone who has not done much drawing in the past, let alone 3D modeling, this has been and will continue to be one of the more difficult aspects of developing this game for some time. Luckily, there are some amazing resources and I have
even more amazing and talented artists who have been helping me to learn the secrets of art along the way.

### What's Next?

<img alt="KTS Truck Sunset" src="/images/KeiSimSunset_content.webp"/>

Overall, I am very happy with the work that has been done on this game so far. From designing what I believe to be a fairly unique game from the ground up, to doing academic research on creating a sorting algorithm, to dipping my toes into the
world of 3D art, I think this game has a strong foundation to continue to develop on. Next on the list is refining the systems already in place (especially the sorting algorithm), and beginning to focus more on the world-building and
story of the game to start laying out the game map. I am very excited to see what lies in the future for this game and I can't wait to share it when the time comes!
