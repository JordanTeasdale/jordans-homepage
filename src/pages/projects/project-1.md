---
layout: ../../layouts/MarkdownPostLayout.astro
title: "Runway Projects"
description: ""
image:
  url: "/images/BulletHellVFX_thumbnail.webp"
  alt: "Gateware Logo"
---
<!-- Description
A collection of UE5 projects developed for Full Sail Labs summer camps meant to help teach teens about game development.
-->

## What are these projects?
During my time with Runway, I was tasked with created four projects in Unreal Engine 5.1, and while I was given a list of required core features, 
I was given a great deal of freedom for adding extra features and functionality. The goal of these projects is for the students to be able to use them
as a reference for when they create their own games and be able to pull features from the projects and modify them to better meet their needs. The four 
projects I created were 1) a platformer, 2) a puzzle, 3) a stealth, and 4) a bullet hell game.

## Modular 3D Platformer

<video autoplay muted loop disablepictureinpicture>
            <source src="/videos/PlatformerShowcase.mp4" type="video/mp4"/>
            <source src="mov_bbb.ogg" type="video/ogg">
        </video>

The platformer project was the first project that I worked on and ended up having some of the simpler core requirements. It is a simple third-person
platformer that had healing and damaging objects and a customizeable camera system. The camera system was required to have 5 fixed positions (left, right
back, front, and top) and I added an additional camera state of a free-moving camera. While this was easy enough to add initially, the camera system ended up
creating issues with another feature created down the line. Some of the additional features I added were, collectible coins, basic AI enemies, projectile obstacels,
customizeable moving platforms, and a grappling hook.

<video autoplay muted loop disablepictureinpicture>
            <source src="/videos/PlatformerSwing.mp4" type="video/mp4"/>
            <source src="mov_bbb.ogg" type="video/ogg">
        </video>

In all of these projects, there is one main feature or aspect of the project that I am particularly proud of. For this project, it is the grappling hook! Before starting
this contract, I had beem playing a good bit of Insomniac's Spider-Man 2 and I had been wanting to emulate the game's swinging system for a while and I thought this to be
the perfect opportunity. And while it was inspired by Spider-Man swinging, the grappling hook was designed more for the purpose of crossing gaps as opposed to it being the 
primary form of movement as it is in the Spider-Man games. The system just shoots a line trace up to the ceiling in front of the player to see if the ceiling is close enough;
if so, then a physics constraint is created, movement is disabled, a rope visual is created, and the player goes swinging. It took several iterations to land on this design and 
it caused the camera system to be rebuilt from the ground up be be compatible with it, but I would say the final result made it all worth it.

## Modular 3D Puzzle

<img alt="Puzzle Showcase" src="/images/PuzzleShowcase_content.webp"/>

The second project I worked on was the puzzle game. The primary goal of this project was to create obstacles which the player would have to get past such as doors and
elevators. The player has to get past these obstacles by identifying the correct passcode to unlock, finding batteries to power, or a combination of the two methods. Being a
puzzle game, I felt that having a first-person camera would make more sense and this allowed me to give players the ability to individually click on the buttons to input the passcode
into the numpad to create a more immersive experience.

Although all of these projects are modular, I am particularly proud of the modularity of this project. In this project, I was required to two door types, one that required a passcode to be
entered on the keypad and one that required a battery to be placed into a battery slot, and a elevator with the same battery slot. But because I created the battery and numpad functionality
separate from the doors and elevators, it made it extremely easy to not just attach either of those features to either the door or the elevator, but also add them both to the same object so
that they can operate in conjunction together. This has made it extremely easy to create multiple variants of both the doors and elevators in a short amount of time. And since they are so
easy to implement onto other objects, multiple variants of an object can be created with minimal effort by just adding in one new object.

## Modular 3D Stealth

<img alt="Stealth Throw Distraction" src="/images/StealthThrow_content.webp"/>

The third project, the stealth game, is the project that I ended up putting the most amount of time into. This project is heavily reliant not AI Behaviour trees to allow the enemy AI's 
to accurately detect the player. But the interaction isn't just one sided. The player is also able to sneek up on enemies to silently take them out. Functionality was also added to allow
the player to throw small rocks which can distract enemies and temporarily pull them away from their standar patrolling path.

<video autoplay muted loop disablepictureinpicture>
            <source src="/videos/StealthAIBehaviour.mp4" type="video/mp4"/>
            <source src="mov_bbb.ogg" type="video/ogg">
        </video>

The main feature in this game that required all those hours was of course the AI behaviours. I wanted to make sure that not only could the enemies detect the player, but also properly
react to the player's actions to chase the player while keeping an appropriate and adjustable distance. I also wished to convey each enemies detection state as well as the player's total
detection level clearly to player so that they can react to their situation accordingly.

## Modular 3D Bullet Hell

<img alt="Bullet Hell Screen Detector" src="/images/BulletHellScreenDetector_content.webp"/>

The final bullet hell game was one of the more unique games out of the bunch. Being that it was a top down shooter and a lot of functionality revolved around when and what was on screen,
so I had to create a system that would detect when objects would enter and exit the screen dynamically based off of the camera's distance from the player and the camera's FOV and aspect
ratio. This was because the player had to automatically shoot only when enemies were on screen and in return enemies would only shoot if they were on screen.

<img alt="Bullet Hell VFX" src="/images/BulletHellVFX_content.webp"/>

I think what really makes this project stand out from the others is the visuals. This game has three basic characters (player, ranged enemy, and melee enemy) each requiring their own unique 
animations and animation blueprint. Finding the correct animations and making sure they would smoothly blend together is something I hadn't done as in-depth before in an UE project before a
learned a great deal about UE's animation system in the process. And speaking of learning, I used Unreal Engine's Niagra particle system to create the VFX for the special attack. And I was
able to take what I learned there to create the arc path visual for the throwable distractions in the stealth game.

## The End Result
At the end of it all, not only did I learn a lot about creating a wide variety of games in Unreal Engine, but I also created projects that will help the next generation to learn a bit more
about what goes on behind the scenes and hopefully inspire some of them to pursue a career in game development. It was a great experience and I can't wait to build off of what I learned
doing it.

